<?php

/**
 * @version        2.3.1
 * @package        Joomla
 * @subpackage     Membership Pro
 * @author         @author     Arjen Schipmolder <schipmolder@gmail.com>
 * @copyright      Arjen Schipmolder
 * @license        GNU/GPL, see LICENSE.php
 */

/**
 * DutyMan - A class intended as a simple example of communication with the DutyMan SOAP API
 *
 * This class has been written as an alternative to DutyMan's VB API testbed application
 * as we're not all working with Windows and VB.
 *
 * The official API documentation ca be found here: http://www.dutyman.biz/documents/dmapi.pdf
 *
 * It uses curl instead of the built-in SOAP libraries because the PHP SOAP libraries caused several problems
 * when creating the SOAP envelope, especially when nesting the 'inDoc' XML inside the SOAP XML.
 */

defined('_JEXEC') or die;

define("DUTYMAN_API_ENDPOINT", "http://www.dutyman.biz/api/dutyman.asmx");
define("DUTYMAN_API_NS", "http://www.dutyman.biz/api/");
define("DUTYMAN_API_TIMEOUT", 10);

class DutyManAPI
{
	private $rosterId;
	private $dbPassword;

	public function __construct($rosterId, $dbPassword) {
		$this->rosterId = $rosterId;
		$this->dbPassword = $dbPassword;
	}
	
	/**
	 *	Retrieves an array of members
	 */
	public function getMembersWithFields($fields, $keyType="dbid", $testMode=true) {
		$soapFunction = "read";

		$inDocValue = $this->inDocValue($fields, $keyType, $testMode);
		$soapEnvelope = $this->makeSoapEnvelope($soapFunction, $inDocValue);
		$soapResult = $this->makeSoapCall($soapEnvelope, $soapFunction);

		$soapResultParts = explode("<readResult>", $soapResult);
		$soapResultParts = explode("</readResult>", $soapResultParts[1]);
		$soapReadResult = $soapResultParts[0];

		$soapReadResult = str_replace("&lt;", "<", $soapReadResult);
		$soapReadResult = str_replace("&gt;", ">", $soapReadResult);
		
		$members = $this->membersFromResultXml($soapReadResult);
		return $members;
	}
	
	/**
	 *	Updates a member's details, or creates a new member if the member didn't exist yet
	 */
	public function updateMember($fields, $keyType="dbid", $testMode=true) {
		$soapFunction = "write";

		$inDocValue = $this->inDocValue($fields, $keyType, $testMode);
		$soapEnvelope = $this->makeSoapEnvelope($soapFunction, $inDocValue);
		$soapResult = $this->makeSoapCall($soapEnvelope, $soapFunction);

		$soapResultParts = explode("<writeResult>", $soapResult);
		$soapResultParts = explode("</writeResult>", $soapResultParts[1]);
		$soapWriteResult = $soapResultParts[0];

		$soapWriteResult = str_replace("&lt;", "<", $soapWriteResult);
		$soapWriteResult = str_replace("&gt;", ">", $soapWriteResult);

		$members = $this->membersFromResultXml($soapWriteResult);
		
		return $members;
	}
	
	/**
	 *	Deletes a member
	 */
	public function deleteMember($fields, $keyType="dbid", $testMode=true, $swapWanted=true) {
		$soapFunction = "delete";

		$inDocValue = $this->inDocValue($fields, $keyType, $testMode, $swapWanted);
		$soapEnvelope = $this->makeSoapEnvelope($soapFunction, $inDocValue);
		$soapResult = $this->makeSoapCall($soapEnvelope, $soapFunction);

		$soapResultParts = explode("<deleteResult>", $soapResult);
		$soapResultParts = explode("</deleteResult>", $soapResultParts[1]);
		$soapDeleteResult = $soapResultParts[0];

		$soapDeleteResult = str_replace("&lt;", "<", $soapDeleteResult);
		$soapDeleteResult = str_replace("&gt;", ">", $soapDeleteResult);

		$members = $this->membersFromResultXml($soapDeleteResult);

		return $members;
	}

	private function makeSoapCall($postString, $soapFunction) {
		$soapCurl = curl_init(); 
		curl_setopt($soapCurl, CURLOPT_URL,            DUTYMAN_API_ENDPOINT);   
		curl_setopt($soapCurl, CURLOPT_CONNECTTIMEOUT, DUTYMAN_API_TIMEOUT); 
		curl_setopt($soapCurl, CURLOPT_TIMEOUT,        DUTYMAN_API_TIMEOUT); 
		curl_setopt($soapCurl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($soapCurl, CURLOPT_SSL_VERIFYPEER, false);  
		curl_setopt($soapCurl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($soapCurl, CURLOPT_POST,           true);
		curl_setopt($soapCurl, CURLOPT_POSTFIELDS,     $postString); 
		curl_setopt($soapCurl, CURLOPT_HTTPHEADER,     array('Content-Type: text/xml; charset=UTF-8; action="http://www.dutyman.biz/api/'.$soapFunction.'"', 'Content-Length: '.strlen($postString) )); 
		$result = curl_exec($soapCurl);
		$err = curl_error($soapCurl);
	
		// echo "result: ".$result."<br />";
		// echo "err: ".$err."<br />";
		return $result;
	}
	
	private function makeSoapEnvelope($soapFunction, $inDoc) {
		$soapEnvelope  = "<"."?xml version=\"1.0\" encoding=\"UTF-8\"?".">";
		$soapEnvelope .= "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";
		$soapEnvelope .= "<soap:Body>";
		$soapEnvelope .= "<".$soapFunction." xmlns=\"".DUTYMAN_API_NS."\">";
		$soapEnvelope .= "<inDoc>";
		$soapEnvelope .= $inDoc;
		$soapEnvelope .= "</inDoc>";
		$soapEnvelope .= "</".$soapFunction.">";
		$soapEnvelope .= "</soap:Body>";
		$soapEnvelope .= "</soap:Envelope>";
		return $soapEnvelope;
	}

	private function inDocValue($fields=array(), $keyType="name", $test=true, $swap=true) {
		if ($test)
			$testMode = "yes";
		else
			$testMode = "no";
			
		if ($swap)
			$swapWanted = "yes";
		else
			$swapWanted = "no";
			
		$inDoc  = "<"."?xml version=\"1.0\" encoding=\"utf-8\"?".">";
		$inDoc .= "<dutyman rosterid=\"".$this->rosterId."\" dbpswd=\"".$this->dbPassword."\" testmode=\"".$testMode."\">";
		$inDoc .= "<members keytype=\"".$keyType."\" swapwanted=\"".$swapWanted."\">";
		$inDoc .= "<member>";
		$inDoc .= "<fields>";
		foreach ($fields as $field) {
			$inDoc .= "<field name=\"".$field['fieldName']."\" value=\"".$field['fieldValue']."\" />";
		}
		$inDoc .= "</fields>";
		$inDoc .= "<infos />";
		$inDoc .= "</member>";
		$inDoc .= "</members>";
		$inDoc .= "</dutyman>";

		$inDoc = str_replace("<", "&lt;", $inDoc);
		$inDoc = str_replace(">", "&gt;", $inDoc);
		
		return $inDoc;
	}
	
	private function membersFromResultXml($resultXmlString) {
		$resultXml = simplexml_load_string($resultXmlString);
		
		$members = array();
		$member['fields'] = array();

		foreach($resultXml->members->member as $memberXml) {
			$member = array();
			foreach($memberXml->fields as $fieldXml) {
				$fieldName = (string)$fieldXml->field->attributes()->name;
				$fieldValue = (string)$fieldXml->field->attributes()->value;
				$member['fields'][trim($fieldName)] = trim($fieldValue);
			}
			$member['messages'] = array();
			foreach($memberXml->messages[0]->message as $messagesXml) {
				$messageAction = (string)$messagesXml->attributes()->action;
				$messageResult = (string)$messagesXml->attributes()->result;
				$messageReason = (string)$messagesXml->attributes()->reason;
				$message = array("action"=>trim($messageAction), "result"=>trim($messageResult), "reason"=>trim($messageReason));
				$member['messages'][] = $message;
			}
			$members[] = $member;
		}
		return $members;
	}

}

?>
