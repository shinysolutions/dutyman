# DutyMan Integration Plugin for Membership Pro #

Very briefly: DutyMan is a web based rota tool used by clubs and organisations to share out duties such as running a sports club.  Membership Pro is a Joomla Component from JoomDonations which allows people t subscribe to a website such as a Club.  This software does the following:

* When a member joins using the appropriate Plan they have a DutyMan Account Created.  (Duty man will email through the details to the member)
* When a member lets their membership lapse, their DutyMan Account is deleted.  If they had any duties allocated these are also deleted. A nominaetd email address receives an email to advise them a duty is now needing cover.

### How do I get set up? ###

* Download the plugin.
* Install it in Joomla!
* Set the plugin settings to match the settings in your DutyMan Setup/Security & privacy page to set the database password and the Rosta ID (In the URL you access DutyMan through).  Set an email address to receive any alerts about deleted members who had duties.
* Publish the Plugin
* Go to the Subscription Plans on Membership Pro and for each plan set if joining the plan should create a DutyMan accont, and if Expirign teh plan should delete a plan DutyMan Account.