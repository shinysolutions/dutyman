<?php
/**
 * @version        2.3.1
 * @package        Joomla
 * @subpackage     Membership Pro
 * @author         Calum Polwart
 * @copyright      Copyright (C) 2016 Shiny Solutions
 * @license        GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die;

class plgOSMembershipDutyman extends JPlugin
{

    public function __construct(& $subject, $config)
    {
            parent::__construct($subject, $config);

            JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_osmembership/table');
    }

    /**
     * Render setting form
     *
     * @param PlanOSMembership $row
     *
     * @return array
     */
    public function onEditSubscriptionPlan($row)
    {

            ob_start();
            $this->drawSettingForm($row);
            $form = ob_get_contents();
            ob_end_clean();

            return array('title' => 'DutyMan',
                         'form'  => $form
            );
            

    }

    /**
     * Store setting into database, in this case, use params field of plans table
     *
     * @param PlanOsMembership $row
     * @param bool             $isNew true if create new plan, false if edit
     */
    public function onAfterSaveSubscriptionPlan($context, $row, $data, $isNew)
    {
            
            $params = new JRegistry($row->params);

            $params->set('CreateDutyManAccount',  $data['CreateDutyManAccount']);
            $params->set('DeleteDutyManAccount',  $data['DeleteDutyManAccount']);
            $row->params = $params->toString();

            $row->store();
            
    }

    public function onMembershipActive($row)
    {
        
        $planId = $row->plan_id;  //current plan

        if (empty($planId)) {
            return false;
        }

        if (!file_exists(JPATH_ROOT . '/components/com_osmembership/osmembership.php')) {
            return;
        }

            
        // Get a db connection.
        $db = JFactory::getDbo();

        // Create a new query object.
        $query = $db->getQuery(true);

        $query
                ->select($db->quoteName('params'))
                ->from($db->quoteName('#__osmembership_plans'))
                ->where($db->quoteName('id') . ' = ' . $db->quote($planId));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);

        // Load the results as a list of stdClass objects (see later for more options on retrieving data).
        $result = $db->loadResult();
        $planParams = json_decode($result, true);

        //check if duty man is set
        if (array_key_exists("CreateDutyManAccount", $planParams)) {
            if ($planParams['CreateDutyManAccount'] =="1") {
            
        require_once dirname(__FILE__).'/api/DutyMan.php';
        $plugin = JPluginHelper::getPlugin('osmembership', 'dutyman');
        $params = new JRegistry($plugin->params);
                


        $rosterId = $params->get('roster_id','');
        $dbPassword = $params->get('roster_password','');

	$dutyMan = new DutyManAPI($rosterId, $dbPassword);
	

	
        // Check if a same name member already exists in DutyMan

        // Connect to database to check
        $option = $this->dutyManSQL();
        $dman = JDatabaseDriver::getInstance( $option );
        $query = $dman->getQuery(true);
        $now = JFactory::getDate();

        $query
            ->select($dman->quoteName(array('Member DBID', 'Email Address', 'First Name', 'Last Name' )))
            ->from($dman->quoteName('members'))
                ->where($dman->quoteName('First Name') . ' = '. $dman->quote($row->first_name) . " AND ". $dman->quoteName('Last Name') . ' LIKE '. $dman->quote($row->last_name . "%")  );

        $dman->setQuery($query);

        // Load the results as a list of stdClass objects (see later for more op
        $results = $dman->loadAssocList();

        if (empty($results)) {
            // No member with same name exists
            $memberFirstNameField = array("fieldName"=>"First Name", "fieldValue"=>$row->first_name);
            $memberLastNameField = array("fieldName"=>"Last Name", "fieldValue"=>$row->last_name);
            $memberEmailField = array("fieldName"=>"Email Address", "fieldValue"=>$row->email);
            $memberDbidField = array("fieldName"=>"Member DBID", "fieldValue"=>"J".$row->user_id);
        } else {
            // Member with same / simillar name exists - tread with care!
            // For each returned matching member check if they are the SAME member
            $l = count($results);

            for ($r=0 ; $r < $l; $r++) {
                if ($results[$r]['Member DBID'] == "J".$row->user_id) {
                    // Member exists in DutyMan already - use the opportunity to update records.
                    $memberLastNameField = array("fieldName"=>"Last Name", "fieldValue"=>$row->last_name);

                    break;
                } else {
                    if ($results[$r]['Email Address'] == $row->email) {
                        // Member exists in DutyMan already - use the opportunity to update records.
                        $memberLastNameField = array("fieldName"=>"Last Name", "fieldValue"=>$row->last_name);

                        break;
                    }
                }
            }
            if (!exists($memberLastNameField)) {
                $memberLastNameField = array("fieldName"=>"Last Name", "fieldValue"=>$row->last_name."-".$row->user_id);
            }
            $memberFirstNameField = array("fieldName"=>"First Name", "fieldValue"=>$row->first_name);
            $memberEmailField = array("fieldName"=>"Email Address", "fieldValue"=>$row->email);
            $memberDbidField = array("fieldName"=>"Member DBID", "fieldValue"=>"J".$row->user_id);
        }

        
        
	$fields = array($memberFirstNameField, $memberLastNameField, $memberDbidField, $memberEmailField);
	$members = $dutyMan->updateMember($fields, "dbid", false);
          }
        }
        
        return;
   
    }

    public function onMembershipExpire($row)
    {
        
        
        $planId = $row->plan_id;  //current plan

        if (empty($planId)) {
            return false;
        }

        if (!file_exists(JPATH_ROOT . '/components/com_osmembership/osmembership.php')) {
            return;
        }

        // Get a db connection.
        $db = JFactory::getDbo();

        // Create a new query object.
        $query = $db->getQuery(true);

        $query
                ->select($db->quoteName('params'))
                ->from($db->quoteName('#__osmembership_plans'))
                ->where($db->quoteName('id') . ' = ' . $db->quote($planId));

        // Reset the query using our newly populated query object.
        $db->setQuery($query);

        // Load the results as a list of stdClass objects (see later for more options on retrieving data).
        $result = $db->loadResult();
        $planParams = json_decode($result, true);

        //check if duty man is set
        if (array_key_exists("DeleteDutyManAccount", $planParams)) {
            if ($planParams['DeleteDutyManAccount'] =="1") {

                // Check if the member has any duties scheduled
                // Connect to database to check
                $option = $this->dutyManSQL();
                $dman = JDatabaseDriver::getInstance( $option );
                $query = $dman->getQuery(true);
                $now = JFactory::getDate();

                $query
                    ->select($dman->quoteName(array('Duty Date', 'Duty Time','Event','Duty Type','First Name','Last Name', )))
                    ->from($dman->quoteName('duties'))
                        ->where($dman->quoteName('Member DBID') . ' = '. $dman->quote('J'.$row->user_id) . " AND ". $dman->quoteName('Duty Date') . " > ". $dman->Quote($now) );

                $dman->setQuery($query);

                // Load the results as a list of stdClass objects (see later for more op
                $results = $dman->loadAssocList();

                if (empty($results)) {
                    // No duties exist for member so no warning needs to be sent
                } else {

                    $siteName = JFactory::getApplication()->get('sitename');
                    $l = count($results);


                    $body = "";
                    $body .= "<p>Dear Dutyman Manager,</p>";
                    $body .= "\n";
                    $body .= "<P><b>".$results['0']['First Name']." ".$results['0']['Last Name']."</b> have not renewed their membership at ". $siteName . " and as a result their DutyMan Account has been automatically deleted.\n";
                    $body .= "</P><P><b>They had ".$l. " duties scheduled</b> to be covered - these shifts will need cover arranged.";
                    $body .= "</P>";
                    $body .= "<p>The shifts requiring cover are:</P><ul>";

                    for ($r=0 ; $r < $l; $r++) {
                        $body .= "<li><b>".$results[$r]['Duty Date']." ".$results[$r]['Duty Time']."</b> - ".$results[$r]['Event']." (".$results[$r]['Duty Type'].") </li>";

                    }

                    $body .= "</ul>";
                    $body .= "<p>This message was generated from an automated script on the ".$siteName.' website.</p>';




                    //Send the email to Sail Secretary to alert deleted message        
                    $mailer = JFactory::getMailer();
                    $config = JFactory::getConfig();
                    $sender = array( 
                        $config->get( 'mailfrom' ),
                        $config->get( 'fromname' ) 
                    );
                    $params = new JRegistry($row->params);

                    $mailer->setSender($sender);
                    $recipient =  $params->get('dutyman_email',  $config->get( 'mailfrom' ));


                    $mailer->addRecipient($recipient);

                    $mailer->setSubject('DutyMan - Warning: Membership Cancellation - Duty Deleted');
                    $mailer->isHTML(true);
                    $mailer->Encoding = 'base64';
                    $mailer->setBody($body);

                    $send = $mailer->Send();
                }
            }
            return;           
           
        }
    
        require_once dirname(__FILE__).'/api/DutyMan.php';
        $plugin = JPluginHelper::getPlugin('osmembership', 'dutyman');
        $params = new JRegistry($plugin->params);
                


        $rosterId = $params->get('roster_id','');
        $dbPassword = $params->get('roster_password','');

	$dutyMan = new DutyManAPI($rosterId, $dbPassword);
	
	// Despite of what the official API documentation says, I believe it 
	// returns an error when you add two users with the same name.
	$memberFirstNameField = array("fieldName"=>"First Name", "fieldValue"=>$row->first_name);
	$memberLastNameField = array("fieldName"=>"Last Name", "fieldValue"=>$row->last_name);
        $memberEmailField = array("fieldName"=>"Email Address", "fieldValue"=>$row->email);
	$memberDbidField = array("fieldName"=>"Member DBID", "fieldValue"=>"J".$row->user_id);
	
	$fields = array($memberFirstNameField, $memberLastNameField, $memberDbidField, $memberEmailField);
	$members = $dutyMan->deleteMember($fields, "dbid", false);
    }
    
    
    /**
     * Display form allows users to change settings on subscription plan add/edit screen
     * @param object $row
     */
    private function drawSettingForm($row)
    {
        $params    = new JRegistry($row->params);
        $CreateDutyManAccount = $params->get('CreateDutyManAccount', 'No');  //get the current setting for this plan
        $DeleteDutyManAccount = $params->get('DeleteDutyManAccount', 'No');  //get the current setting for this plan

        jimport( 'joomla.html.html.select' );
        

                
        ?>
        <table class="admintable adminform" style="width: 90%;">
                <tr>
                        <td width="220" class="key">
                                <?php echo JText::_('Create DutyMan Account on Subscription:'); ?>
                        </td>
                        <td>
                            <?php 	echo JHtmlSelect::booleanlist('CreateDutyManAccount','class=\"btn-group btn-group-yesno\"',$CreateDutyManAccount,'Yes','No') ?>
                        </td>
                        <td>
                                <?php echo JText::_('If a DutyMan Account for this member does not exist, should one be created after completion of payment?'); ?>
                        </td>
                </tr>
                
                <tr>
                        <td width="220" class="key">
                                <?php echo JText::_('Delete DutyMan Account on Expiration:'); ?>
                        </td>
                        <td>
                         <?php 	echo JHtmlSelect::booleanlist('DeleteDutyManAccount','class=\"btn-group btn-group-yesno\"',$DeleteDutyManAccount,'Yes','No') ?>

                        </td>
                        <td>
                                <?php echo JText::_('If a DutyMan Account exists for this member, should it be deleted on expiration of this subscription.<b>Caution:</b> Deleting an account deletes any duties the member has been allocated.'); ?>
                        </td>
                </tr>
        </table>
        <?php
    }

    private function dutyManSQL()
    {
        $option = array(); //prevent problems
        $plugin = JPluginHelper::getPlugin('osmembership', 'dutyman');
        $params = new JRegistry($plugin->params);
        $rosterId = $params->get('roster_id','');
        $dbPassword = $params->get('roster_password','');


        $option['driver']   = 'mysql';            // Database driver name
        $option['host']     = 'dutyman.biz:3307';    // Database host name
        $option['user']     = $rosterId;       // User for database authentication
        $option['password'] = $dbPassword;   // Password for database authentication
        $option['database'] = 'dutyman';      // Database name
        $option['prefix']   = '';             // Database prefix (may be empty)


        return ($option);
    }

}